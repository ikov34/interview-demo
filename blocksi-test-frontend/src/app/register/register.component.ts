import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    console.dir(this);
    this.authService.register(this.form).subscribe(
      data => {
        console.dir(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        console.dir(err);
        this.errorMessage = err.error.reason;
        this.isSignUpFailed = true;
      }
    );
  }
}