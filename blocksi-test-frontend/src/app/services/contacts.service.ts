import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://164.8.252.167:3000/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  constructor(private http: HttpClient) { }

  async getContacts(): Promise<any> {
    const data = this.http.get(AUTH_API + 'contacts/', httpOptions).toPromise();
    return data;
  }

  async addContact(contact): Promise<any>{
    const data = this.http.post(AUTH_API + 'contacts/', {contact}, httpOptions).toPromise();
    return data;
  }

  async updateContact(id, contact): Promise<any>{
    const data = this.http.put(AUTH_API + 'contacts/' + id, {contact}, httpOptions).toPromise();
    return data;
  }

  async deleteContact(id): Promise<any> {
    const data = this.http.delete(AUTH_API + 'contacts/' + id, httpOptions).toPromise();
    return data;
  }

}
