import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';


export interface DialogData {
  firstName:string;
  lastName:string;
  address:string;
  town:string;
  postCode:string;
  email:string;
  phoneNumber:string;
}

@Component({
  selector: 'app-edit-contact-dialog',
  templateUrl: './edit-contact-dialog.component.html',
  styleUrls: ['./edit-contact-dialog.component.scss']
})

export class EditContactDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, public dialogRef: MatDialogRef<EditContactDialogComponent>) { }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.data);
  }

}
