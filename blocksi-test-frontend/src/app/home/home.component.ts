import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../services/token-storage.service';
import { ContactsService } from '../services/contacts.service';
import { EditContactDialogComponent } from '../edit-contact-dialog/edit-contact-dialog.component';
import { MatDialog } from '@angular/material/dialog';


declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoggedIn: boolean;
  loading: boolean;
  user: any;
  contacts: any;

  constructor(private tokenStorageService: TokenStorageService, private contactsService: ContactsService,
              public dialog: MatDialog) { }

  async ngOnInit(): Promise<void> {
    try
    {
        this.isLoggedIn = this.tokenStorageService.getToken() != null;
        if(this.isLoggedIn)
        {
          this.loading=true;
          this.user = this.tokenStorageService.getUser();
          this.contacts = (await this.contactsService.getContacts()).contacts;
          this.loading=false;
        }
    }
    catch(err)
    {
        if(err.status == 403)
        {
          this.tokenStorageService.signOut();
          this.isLoggedIn = false;
        }
    }
  }

  /**
   *  Used for displaying the "no contacts" alert
   */
  get hasContacts(): boolean
  {
      return this.contacts != undefined && this.contacts.length > 0;
  }

  /**
   * Editing contacts
   * @param n index of contact in contacts array
   */
  edit(n):void
  {
    const dialogRef = this.dialog.open(EditContactDialogComponent, 
      {
          data: this.contacts[n]
      });

    dialogRef.afterClosed().subscribe(async result => {
      this.loading = true;
      await this.contactsService.updateContact(this.contacts[n]._id, this.contacts[n]);
      this.loading = false;
    });
  }

  /**
   * Deleting contacts
   * @param n  index of contact in contacts array
   */
  async delete(n): Promise<void>
  {
    if(window.confirm(`Sure you want to delete ${this.contacts[n].firstName}?`))
    {
      this.loading = true;
      await this.contactsService.deleteContact(this.contacts[n]._id);
      this.loading = false;
      this.contacts.splice(n,1);
    }
  }

  /**
   * Adding contacts
   */
  async addContact():Promise<void>
  {
    let contact = {firstName:"New", lastName: "contact", address:"add address", town:"add town", postCode:"add postcode", email:"add e-mail", phoneNumber:"add phone-no" };
    
    if(this.contacts == undefined)
    {
        this.contacts = []; 
    }
      

    this.contacts.push(contact);
    this.loading = true;
    await this.contactsService.addContact(contact);
    this.loading = false;
  }

}
