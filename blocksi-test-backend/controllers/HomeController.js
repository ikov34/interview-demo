const AuthService = require('../services/authService'); 
const createError = require('../util/createError');

/**
 *  Public homepage stuff
 */
class HomeController 
{
    constructor()
    {
       this.authService = new AuthService();  
    }

    /*
        Gets all contacts for the currently authenticated user (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async login(req, res, next)
    {
        try 
        {
            let loginSuccess = await this.authService.login(req.body);             
            if(loginSuccess.token != undefined)
            {
                return res.json(loginSuccess);
            }
            else 
            {
                throw loginSuccess;
            }
        }
        catch(err)
        {
            console.warn("Problem with login!"); 
            next(err); 
        }
    }

     /*
        Gets a specific contact (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async register(req, res, next)
    {
        try 
        {
            let registerAttempt = await this.authService.register(req.body); 

            if(registerAttempt === true)
            {
                res.json({success:true});
            }            
            else 
            {
                throw registerAttempt;
            }
            
        }
        catch(err)
        {
            console.warn("Problem with registration!"); 
            next(err); 
        }
    }

}

module.exports = HomeController;