const jwt = require('jsonwebtoken');
const AuthService = require("../services/authService");

/**
 *  Middleware for authentication
 */
class AuthController 
{
    constructor()
    {
       this.authService = new AuthService();  
    }

    /**
        Checks permissions of given operation (see page 1, section API)
        @param opts {*} - Permissions modes 

        Specified modes: 
        loggedIn: restricted to logged in users
        canViewAll: user has the right to view all contacts
        canUpdate: user may update contacts 
        canDelete: user may delete contacts

    */
    checkPermissions(opts={loggedIn:1})
    {
        return async function (req, res, next)
        {

                // Gather the jwt access token from the request header
                const authHeader = req.headers['authorization']
                const token = authHeader && authHeader.split(' ')[1]
                if (token == null) return res.status(401).json({success: false, reason: "forbidden"});               

                jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {

                    if (err) return res.status(403).json({success: false, reason: "unauthorized"});       
                    req.user = user;

                    delete opts["loggedIn"];

                    // Check user constraints 
                    for(let permission in opts)
                    {
                        if(!req.user[permission] == true)
                        {
                            return res.status(403).json({success: false, reason: "unauthorized"});      
                        }
                    }

                    next();
                });
        }
    }

}

module.exports = AuthController;