const ContactsService = require("../services/contactsService");  

/**
 *  Contacts CRUD
 */
class ContactsController 
{
    constructor()
    {
       this.contactsService = new ContactsService(); 
    }

    /*
        Gets all contacts for the currently authenticated user (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async getAll(req, res, next)
    {
        try 
        {            
            let results = await this.contactsService.getAllForUser(req.user); 
            return res.json(results.contacts ? {success:true, contacts: results.contacts} : {success:false});
        }
        catch(err)
        {
            console.warn("Problem with fetching all contacts!"); 
            next(err); 
        }
    }

     /*
        Gets a specific contact (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async getById(req, res, next)
    {
        try 
        {
            let result = await this.contactsService.getContactById(req.params.id); 
            return res.json(result.contacts[0]); // using aggregate with unwind and projection is cleaner, but perhaps slower, so this does the trick. 
        }
        catch(err)
        {
            console.warn(`Problem with fetching contact ${req.params.id}!`); 
            next(err); 
        }
    }

    
    /*
        Updates a specific contact (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
   async add(req, res, next)
   {
       try 
       {
           this.contactsService.addContact(req.user._id, req.body.contact); 
           res.status(200).end();
       }
       catch(err)
       {
           next(err); 
       }
   }


    /*
        Updates a specific contact (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async updateById(req, res, next)
    {
        try 
        {
            this.contactsService.updateContactById(req.params.id, req.body.contact); 
            res.status(200).end();
        }
        catch(err)
        {
            next(err); 
        }
    }

   /*
       Updates a specific contact (see page 1, section API)
        @param req Express.js request object
        @param res Express.js response object
        @param next middleware callback
    */
    async deleteById(req, res, next)
    {
        try 
        {
            this.contactsService.deleteContactById(req.user._id, req.params.id); 
            res.status(200).end();

        }
        catch(err)
        {
            next(err); 
        }
    }
}

module.exports = ContactsController;