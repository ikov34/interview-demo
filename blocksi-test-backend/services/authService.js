const Service = require('./service'); 
const createError = require("../util/createError");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/**
 *  Authentication helper 
 */
class AuthService extends Service
{
    constructor()
    {
        super(process.env.DB_HOST, process.env.DB_PASS); 
    }

    /**
     * Registers new user 
     * @param {*} userData 
     */
    async register(userData)
    {
        try 
        {
            if( !userData || !userData.username || !userData.password)
            {
                return createError(400, "bad request");
            }
            else
            {
                let countUsers = await this.getDbAndExec(process.env.DB_NAME, async function(db)                
                {
                    let count = await db.collection("users").countDocuments({username: userData.username}); // note: this should be done with uniqueIndex in the database... 
                    
                    if(count == 0)
                    {
                        await db.collection("users").insertOne({username: userData.username, password: bcrypt.hashSync(userData.password, 12)}); 
                    }
                        
                    return count;
                });

                if(countUsers > 0)
                {
                    return createError(500, `User with that username already exists!`);
                }
                else                 
                {
                    return true;
                }

            }
        }
        catch(err) 
        {
            return createError(500, "problem");
        }
        
    }


    /**
     * Logins and generates JWT 
     * @param {*} userData 
     */
    async login(userData)
    {
        try 
        {
            if( !userData || !userData.username || !userData.password)
            {
                return createError(403, "bad request");
            }
            else
            {
                let user = await this.getDbAndExec(process.env.DB_NAME, async function(db)
                {                        
                    return await db.collection("users").findOne({username: userData.username});
                });

                if(user == undefined || user == null)
                {
                    return createError(404, `user ${userData.username} not found!`); 
                }
                else 
                {
                    if(bcrypt.compareSync(userData.password, user.password)) // Auth successful 
                    {
                        let token = jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: `${process.env.LOGIN_SESSION_TIMEOUT}s` });
                
                        user.token = token;
                        return user; 
                    }
                    else 
                    {
                        return createError(403, `wrong username and password`); 
                    }
                }
            }
        }   
        catch(err)
        {
            return createError(500, err.message);
        }     
    }
}

module.exports = AuthService;