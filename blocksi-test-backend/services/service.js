const MongoClient = require('mongodb').MongoClient;

class Service 
{
    constructor(dbHost, dbPort)
    {
         this.dbHost = dbHost;  // Hostname or IP 
         this.dbPort = dbPort;  // MongoDB port 
         this.client = null;
    }

    /**
     * Helper function for getting certain Database and executing querys
     * @param {*} name - Database name 
     * @param {*} cb - callback function for querying / non-querying database 
     */
    async getDbAndExec(name, cb) 
    {
        try 
        {
            if(this.client == null)
                this.client = await MongoClient.connect(`mongodb://${this.dbHost}:${this.dbPort}/${name}`, {useUnifiedTopology:true});

            let db = this.client.db(name);

            let results = null; 

            try 
            {
                results = await cb(db);
            }
            catch(err)
            {
                throw err;
            }
            
            return results;        
        }
        catch(outerErr)
        {
            throw outerErr; 
        }
       
    }
}

module.exports = Service;