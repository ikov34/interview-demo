const Service = require('./service'); 

/**
 *  Contacts CRUD helper 
 */
class ContactsService extends Service
{
    constructor()
    {
        super(process.env.DB_HOST, process.env.DB_PASS); 
    }

    async getAllForUser(user)
    {
        return await this.getDbAndExec(process.env.DB_NAME, async db => 
        {
            return await db.collection("users").findOne({_id: require("mongodb").ObjectId(user._id)}, {projection:{_id:1, contacts:1}});
        }); 
    }

    async getContactById(id)
    {
        return await this.getDbAndExec(process.env.DB_NAME, async db => 
        {
            return await db.collection("users").findOne({"contacts._id": require("mongodb").ObjectId(id)},{projection:{_id:1, contacts:1}});
        }); 
    }


    async addContact(userId, contact)
    {
        let id = require("mongodb").ObjectId();

        await this.getDbAndExec(process.env.DB_NAME, async db => 
            {

                await db.collection("users").updateOne({"_id": require("mongodb").ObjectId(userId)},{$push:
                    {contacts: { 
                        "_id": id,
                        "firstName": contact.firstName,
                        "lastName": contact.lastName,
                        "address": contact.address,
                        "town": contact.town,
                        "postCode": contact.postCode,
                        "email": contact.email,
                        "phoneNumber": contact.phoneNumber
                    }}
                }); 
                // NOTE: What I did here is not something I would normally do. In production environments, the contact JSON object would be validated before update, to prevent abuse/incorrect use of the API. A return null/false would be in order, after which a 400 Bad Request response. 
            }); 
    
            return id; 
    }

    async updateContactById(id, contact) 
    {    
        
        await this.getDbAndExec(process.env.DB_NAME, async db => 
        {
            await db.collection("users").updateOne({"contacts._id": require("mongodb").ObjectId(id)},{$set: 
            { 
                "contacts.$.firstName": contact.firstName,
                "contacts.$.lastName": contact.lastName,
                "contacts.$.address": contact.address,
                "contacts.$.town": contact.town,
                "contacts.$.postCode": contact.postCode,
                "contacts.$.email": contact.email,
                "contacts.$.phoneNumber": contact.phoneNumber
            }}); 
            // NOTE: What I did here is not something I would normally do. In production environments, the contact JSON object would be validated before update, to prevent abuse/incorrect use of the API. A return null/false would be in order, after which a 400 Bad Request response. 
        }); 

        return true; 
    }

    
    async deleteContactById(userId, id) 
    {    
        try 
        {
            await this.getDbAndExec(process.env.DB_NAME, async db => 
            {
                await db.collection("users").updateOne({"_id": require("mongodb").ObjectId(userId)},{$pull: 
                {
                    "contacts": {"_id": require("mongodb").ObjectId(id)}
                }}); 
            }); 

            return true;    

        }
        catch(err)
        {
            console.warn(`Problem deleteing contact ${id} for user ${userId}!`);
            return false;
        }
    }
}

module.exports = ContactsService;