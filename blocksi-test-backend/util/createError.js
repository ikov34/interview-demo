module.exports = function(code, reason)
{
    let err = new Error(reason); 
    err.code = code; 
    return err; 
}