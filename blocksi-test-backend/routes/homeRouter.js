const express = require('express'); 
const router = express.Router(); 

const HomeController = require('../controllers/HomeController');

/* CRUD routes - see test page 1, section API */

router.post("/login", (req, res, next) => new HomeController().login(req,res,next)); 
router.post("/register", (req, res, next) => new HomeController().register(req,res,next)); 

module.exports = router; 