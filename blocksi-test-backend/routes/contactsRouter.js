const express = require('express'); 
const router = express.Router(); 

const ContactsController = require('../controllers/ContactsController');
const AuthController = require('../controllers/AuthController');

/* CRUD routes - see test page 1, section API */

router.get("/", new AuthController().checkPermissions(),  (req,res,next) => new ContactsController().getAll(req,res,next)); 
router.get("/:id", new AuthController().checkPermissions(), (req,res,next) => new ContactsController().getById(req,res,next)); 
router.post("/", new AuthController().checkPermissions(), (req,res,next) =>  new ContactsController().add(req,res,next)); 
router.put("/:id", new AuthController().checkPermissions(), (req,res,next) =>  new ContactsController().updateById(req,res,next)); 
router.delete("/:id", new AuthController().checkPermissions(), (req,res,next) =>new ContactsController().deleteById(req,res,next)); 

module.exports = router; 