const express = require('express')
const app = express();
const bodyParser = require('body-parser');
const createError = require('./util/createError');
const morgan = require('morgan');
const cors = require('cors');

require('dotenv').config(); // init .env 


// Initial middleware
app.use(morgan('dev'));
app.use(bodyParser.json());


app.use(cors());

/* Routes */
const contactsRouter = require("./routes/contactsRouter"); 
const homeRouter = require("./routes/homeRouter"); 

app.get("/test", (req,res)=>{res.json({test:true})});

app.options('*', cors());
app.use("/", homeRouter);
app.use("/contacts", contactsRouter);

// Not found
app.use(function(req, res, next) {
    res.status(404).json({success: false, reason:`Route ${req.method} ${req.url} not found!`});
});

app.use((err, req, res, next) => 
{
    res.status(err.code).json({success: false, reason: err.message});
});


app.listen(process.env.APP_LISTEN, () => {console.log(`Blocksi backend API started on ${process.env.APP_LISTEN}`);}); 